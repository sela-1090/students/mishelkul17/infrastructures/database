# Database
## Deploy postgress



## Create database namespace:

~~~
kubectl create namespace database
~~~

## Install the PostgreSQL chart from Bitnami using Helm:

~~~
helm install my-release oci://registry-1.docker.io/bitnamicharts/postgresql --namespace database
~~~

### Added the "azure-marketplace" Helm repository:
~~~
helm repo add azure-marketplace https://marketplace.azurecr.io/helm/v1/repo
~~~

### Update:
~~~
helm repo update
~~~

## Password for the postgress:

~~~
kubectl get secret --namespace database my-release-postgresql -o jsonpath="{.data.postgres-password}" | base64 --decode
~~~

## Connect to postgress:

~~~
kubectl run my-release-postgresql-client --rm --tty -i --restart='Never' --namespace database --image marketplace.azurecr.io/bitnami/postgresql:15.1.0-debian-11-r0 --env="PGPASSWORD=<output from previous command>" --command -- psql --host my-release-postgresql -U postgres -d postgres -p 5432
~~~

## Port forward:

~~~
kubectl port-forward --namespace database svc/my-release-postgresql 5432:5432
~~~

### Create a database:

~~~
CREATE DATABASE resume;
~~~ 

### Connect to resume database:

~~~
kubectl exec -it my-release-postgresql-0 --namespace database -- psql --host my-release-postgresql -U postgres -d resume -p 5432
~~~

## Delete postgresql:
~~~
helm delete my-release oci://registry-1.docker.io/bitnamicharts/postgresql --namespace database

kubectl delete namespace database
~~~
